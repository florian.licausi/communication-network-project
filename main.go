package main

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/adaptor/v2"
	"github.com/gofiber/fiber/v2"
	"log"
	"math/rand"
	"net/http"
	"strings"
)

// Users Users list
var Users = make(map[string]*User)

// UserTkn Users token list
var UserTkn = make(map[string]string)

type Message struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Content string `json:"content"`
}

// User structure
type User struct {
	Id        string `json:"username"`
	Name      string `json:"name"`
	password  string
	messages  *map[string][]Message
	keys      map[string]string
	listeners []*chan Message
}

// Create a user listener
func registerListener(c *User) chan Message {
	fmt.Println("Register listener")
	ch := make(chan Message, 100) //equivalent to malloc in C
	c.listeners = append(c.listeners, &ch)
	return ch
}

// Send a message to all user listners
func (c *User) sendToListeners(m Message) {
	fmt.Println("Send to listener")
	for i, listener := range c.listeners {
		fmt.Println("listener", i)
		*listener <- m
	}
}

// Alphabetic letters
var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// Generate random string of n letters
func randSeq(n int) string {
	b := make([]rune, n) // rune is equivalent to 'char' in C
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func main() {
	msg := make(map[string][]Message)

	// Users created at start
	Users["Nikita"] = &User{
		Id:       "Nikita",
		Name:     "FAESCH Nikita",
		password: "f8cf7194eb53f512b56990ef7a460a83",
		messages: &msg,
		keys:     make(map[string]string),
	}

	msg = make(map[string][]Message)

	Users["Vladyslav"] = &User{
		Id:       "Vladyslav",
		Name:     "SHULHACH Vladyslav",
		password: "67b926ea2a952e2c0e5179089b30d902",
		messages: &msg,
		keys:     make(map[string]string),
	}

	app := fiber.New()//web framework allowing the implementation of http in go

	// Redirect to login by default
	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.Redirect("/login", 302)
	}) //where ctx = server context with the headers, body etc

	// Return index page if login is OK
	app.Get("/tkn::tkn", func(ctx *fiber.Ctx) error {
		tkn := ctx.Params("tkn")

		if _, ok := UserTkn[tkn]; ok {
			return ctx.SendFile("./index.html")
		}

		return ctx.Redirect("/login", 302)
	})

	// Login form submit
	app.Post("/login_action", func(ctx *fiber.Ctx) error {
		// Get form values
		username := ctx.FormValue("username")
		password := ctx.FormValue("password")

		if usr, ok := Users[username]; ok {
			if password == usr.password {
				tkn := randSeq(10)
				UserTkn[tkn] = usr.Id
				return ctx.Redirect("/tkn:"+tkn, 302)
			}
		}
		return ctx.Redirect("/login?wrong=true", 302)
	})

	// Register form submit
	app.Post("/register_action", func(ctx *fiber.Ctx) error {
		// Get form values
		name := ctx.FormValue("name")
		username := ctx.FormValue("username")
		password := ctx.FormValue("password")
		password_confirm := ctx.FormValue("password_confirm")

		// If password and confirm password isn't the same
		check_pass:= password != password_confirm
		if(check_pass) {
			return ctx.Redirect("/register?wrong=true", 302)
		}


		// If the user doesn't exist
		if _, ok := Users[username]; !ok {
			msg := make(map[string][]Message)
			Users[username] = &User{
				Id:       username,
				Name:     name,
				password: password,
				messages: &msg,
				keys:     make(map[string]string),
			}

			// Token is a random string of 10 char
			tkn := randSeq(10)
			UserTkn[tkn] = username
			// Redirect to index with the token
			return ctx.Redirect("/tkn:"+tkn, 302)
		}

		// If the user exists, redirect to login page.
		return ctx.Redirect("/login?exists=true", 302)
	})

	// Login HTML page
	app.Get("/login", func(ctx *fiber.Ctx) error {
		return ctx.SendFile("./login.html")
	})

	// Register HTML page
	app.Get("/register", func(ctx *fiber.Ctx) error {
		return ctx.SendFile("./register.html")
	})

	// Send message to the target. All target opened tab will receive this message.
	app.Post("/send_message/tkn::tkn/target::target", func(ctx *fiber.Ctx) error {
		tkn := ctx.Params("tkn")
		targetId := ctx.Params("target")
		var message map[string]string
		// Get request body (which contains message)
		_ = json.Unmarshal(ctx.Body(), &message)

		if usrId, ok := UserTkn[tkn]; ok {
			// Create a message object and send it to all listeners of this target
			m := Message{
				From:    usrId,
				To:      targetId,
				Content: message["content"],
			}
			if tgt, ok := Users[targetId]; ok {
				tgt.sendToListeners(m)
			}
			resJson, _ := json.Marshal(m)
			return ctx.Send(resJson)
		}

		// If unconnected, redirect to login
		return ctx.Redirect("/login", 302)
	})

	// Get current user informations
	app.Get("/whoami/tkn::tkn", func(ctx *fiber.Ctx) error {
		tkn := ctx.Params("tkn")

//usertkn[tkn]= user that has the token tkn sent by client in his request and that server retrieves from context
//if userId is the usertkn[tkn] 
		if usrId, ok := UserTkn[tkn]; ok {
			usr := Users[usrId]
			// Used in Javascript on UI to know who you are
			res := map[string]string{
				"name":     usr.Name,
				"username": usr.Id,
				"token":    tkn,
			}
			resJson, _ := json.Marshal(res)// parse res which has 3 fields in a json. resJson is a list of bytes that contains the Json data and _ is nil if all goes well
			return ctx.Send(resJson)
		}

		// If unconnected, return to login
		return ctx.Redirect("/login", 302)
	})

	// Get all messages from history
	app.Get("/messages/tkn::tkn/target::target", func(ctx *fiber.Ctx) error {
		tkn := ctx.Params("tkn")
		targetId := ctx.Params("target")

		if usrId, ok := UserTkn[tkn]; ok {
			messages := Users[usrId].messages
			if msgs, ok := (*messages)[targetId]; ok {
				resJson, _ := json.Marshal(msgs)
				return ctx.Send(resJson)
			} else {
				resJson, _ := json.Marshal(make([]Message, 0))
				return ctx.Send(resJson)
			}
		}

		// If unconnected, redirect to login
		return ctx.Redirect("/login", 302)
	})

	app.Get("/receive/tkn::tkn", adaptor.HTTPHandler(http.HandlerFunc(messageHandler)))

	// Get all users
	app.Get("/users", func(ctx *fiber.Ctx) error {
		users := make([]User, 0)

		for _, client := range Users {
			users = append(users, *client)
		}

		// Transform users array to JSON array
		resJson, _ := json.Marshal(users)

		return ctx.Send(resJson)
	})

	// Listen on port 3000
	err := app.Listen(":3000")
	if err != nil {
		log.Fatal(err)
	}
}

// Receive message handler
func messageHandler(w http.ResponseWriter, r *http.Request) {
	// Set HTTP headers to tell to browser that it's a stream
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	// Take token from URL and associated user
	tkn := strings.Split(r.URL.String(), ":")[1]
	userId, ok := UserTkn[tkn]

	// If no user where found, return 401 (unauthorized)
	if !ok {
		w.WriteHeader(401)
		return
	}

	// Listen for message, transform to JSON
	m := <-registerListener(Users[userId])

	jsonStr, err := json.Marshal(m)
	if err != nil {
		fmt.Println(fmt.Errorf("Failed to convert in JSON user message %v\n", m))
	}

	_, err = fmt.Fprintf(w, "data: %v\n\n", string(jsonStr))
	if err != nil {
		fmt.Println(fmt.Errorf("Failed to send message %s\n", string(jsonStr)))
	}

	// Send to client
	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
}
